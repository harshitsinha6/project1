



import os
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging (1)
import pathlib
import tensorflow as tf
import argparse
import cv2

import numpy as np
#from PIL import Image
#import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings

from tensorflow.python.saved_model import tag_constants


gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

CAP = cv2.VideoCapture("/tf-serving/tresspass1.mp4")

import time
#from object_detection.utils import label_map_util, visualization_utils as viz_utils
#from object_detection.protos import string_int_label_map_pb2


# PROVIDE PATH TO IMAGE DIRECTORY
#IMAGE_PATHS = 'C:/Users/Pratyush/Documents/Object-detection/img.jpg'

# PROVIDE PATH TO MODEL DIRECTORY
PATH_TO_MODEL_DIR = '/tf-serving/saved_model/1'
# C:\Users\Pratyush\Documents\Object-detection\faster_rcnn_resnet50_v1_640x640_coco17_tpu-8
#PATH_TO_MODEL_DIR = 'C:/Users/Pratyush/Documents/Object-detection/ssd_mobilenet_v1_coco_2018_01_28'

# PROVIDE PATH TO LABEL MAP
#PATH_TO_LABELS = 'C:/Users/Pratyush/Documents/Object-detection/label_map.pbtxt'

# PROVIDE THE MINIMUM CONFIDENCE THRESHOLD
MIN_CONF_THRESH = float(0.60)

# LOAD THE MODEL
PATH_TO_SAVED_MODEL = PATH_TO_MODEL_DIR


#category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,use_display_name=True)

print('Loading model...', end='')
start_time = time.time()

# model = tf.saved_model.load_v2(PATH_TO_SAVED_MODEL)
# detect_fn = model.signatures['serving_default']
detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL, tags=[tag_constants.SERVING])
detect_fn = detect_fn.signatures['serving_default']
end_time = time.time()
elapsed_time = end_time - start_time
print('Done! Took {} seconds'.format(elapsed_time))



def inference_model(image, Id):
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_expanded = np.expand_dims(image_rgb, axis=0)
    input_tensor = tf.convert_to_tensor(image)
    input_tensor = input_tensor[tf.newaxis, ...]
    start_time = time.time()
    detections = detect_fn(input_tensor)
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f"fps: {1/elapsed_time} \t: elapsed time: {elapsed_time} \t: detections: {detections.keys()}")
    """num_detections = int(detections.pop('num_detections'))
    detections = {key: value[0, :num_detections].numpy()
                   for key, value in detections.items()}
    detections['num_detections'] = num_detections

    # detection_classes should be ints.
    detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

    image_with_detections = image.copy()

    # SET MIN_SCORE_THRESH BASED ON YOU MINIMUM THRESHOLD FOR DETECTIONS
    viz_utils.visualize_boxes_and_labels_on_image_array(
          image_with_detections,
          detections['detection_boxes'],
          detections['detection_classes'],
          detections['detection_scores'],
          category_index,
          use_normalized_coordinates=True,
          max_boxes_to_draw=200,
          min_score_thresh=0.5,
          agnostic_mode=False)
    return image_with_detections, elapsed_time"""
    return detections,1


def run(Id, CAP):
    print('Capturing the frame: ')
    
    start_time = time.time()
    counter = 0
    total_seconds = 0
    fps=0
    while True:
        ret, frame = CAP.read()
        if ret:
            image_with_detections, seconds = inference_model(frame, Id)
            counter+=1
            total_seconds += seconds
        fps = 1/seconds
        if counter==20:
            break
        #cv2.putText(image_with_detections, "fps: " + str(fps), (50,50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,0), 2, cv2.LINE_AA)
        #cv2.imshow("image", image_with_detections)
        #if cv2.waitKey(1) & 0xFF==ord('q'):
        #    break
    
    end_time = time.time()
    elapsed_time = end_time - start_time
        
    print('Done! Took {} seconds'.format(elapsed_time))
        
    CAP.release()
    cv2.destroyAllWindows()
        
run(1, CAP)






















