

import tensorflow as tf
import cv2, numpy as np
import json, time
import requests as http

CAP = cv2.VideoCapture("/tf-serving/tresspass1.mp4")

def createBatches(cap):
    batches = []
    batch=[]
    batch_size = 1
    counter=1
    while True:
        ret, img = cap.read()
        if not ret:
            break
        img = cv2.resize(img, (256, 256))
        batch.append(img.tolist()) 
        if counter%batch_size==0:
            batches.append(batch)
            batch=[]
        counter+=1
        if counter == 100:
            break
    return batches
    
def tensorflowServing(batch):
    def rest_requests(text, url=None):
        if url==None:
            url = "http://localhost:8601/v1/models/object_detection/versions/1:predict"
        payload = json.dumps({
            "instances": [ t for t in text ]
            })
        stime = time.time()
        response = http.post(url, payload)
        etime = time.time()
        print(f"fps: {1/(etime-stime)}, \t elapsed time: {etime-stime}")
        return response, etime-stime
    rs, t = rest_requests(text=batch)
    return rs, t

batches = createBatches(CAP)
tot_time = 0
pred_response = []
for batch in batches:
    rs, t = tensorflowServing(batch)
    print(rs, rs.json().keys())
    pred_response.append(rs)
    tot_time += t
    
print(f"total time taken is: {tot_time}")
